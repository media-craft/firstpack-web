<?php
/**
 * @file	ログインコントローラー
 * @author	kanda@media-craft.co.jp
 * @date	2018/02/17
 * @version	1.00
 * @note	ログイン時のリクエストを振り分ける
 */

require_once("Controller/baseController.php");
require_once("Model/Data.php");

class dataController extends baseController {

	/**
	 * コンストラクタ
	 */
	public function __construct(){
		parent::__construct();
	}
	/**
	 * デストラクタ
	 */
	public function __destruct(){
	}
	/**
	 * 在庫管理表表示
	 * @return    なし
	 */
	public function stocklistAction() {
		$title = SystemConfig::$SYSNAME;

    //商品区分2の取得
    require_once("Model/Meisyou.php");
    $meisyou = new Meisyou();
    $syoKbn2Arr =$meisyou->getSyohinKbn2();
    $zaiKbnArr[1] = "在庫管理する商品";
    $zaiKbnArr[0] = "在庫管理しない商品";

    require_once("Lib/Session.php");
    $session = new Session();
    $session->start();
    $siten_cd = $session->get('siten_cd');

		//Viewの呼出
		include("./View/data/stocklist.php");
	}
  /**
   * 在庫管理一覧データの取得
   * @return json
   */
  public function getstocklistAction() {
    global $LOG;
    $LOG->trace('ymd:'.$_POST['ymd']);
    $arr[] = "toku_cd";
    $arr[] = "syo_kbn2";
    $arr[] = "zaiko_kbn";
    $arr[] = "ymd";
    $where = array();
    $where['siten_cd'] = $_POST['siten_cd'];
    foreach($arr as $key => $value) {
      if($_POST[$value]) {
        if($value=="ymd") {
          $ymd = str_replace("/","",str_replace("-","",$_POST['ymd']));
          $where[$value] = $ymd;
        } else {
          $where[$value] = $_POST[$value];
        }
      }
    }
    $data = new Data();
    $list = $data->getStockList($where);

    //print_r($where);

    $suu_kbn = $_POST['suu_kbn'];

    //jsonの生成
    $json = '';
    $json .= '{';
    $json .= '"head":{';
    $json .= '"status":1';
    $json .= '}';
    $json .= ',"data":[';
    $index = 1;
    foreach($list as $key => $value) {
      if($suu_kbn > 0 && $value['ZAIKO'] < 1) {
        //数量区分が1の場合、在庫がある商品のみ
        continue;
      }
      if($index!=1) {
        $json .= ',';
      }
      $json .= '{';
      $json .= '"syo_cd":"'.$value['CD'].'"';
      $json .= ',"hinmei":"'.$this->escapeHinmei(mb_convert_encoding($value['HINMEI'],"utf-8","SJIS-win")).'"';
      $json .= ',"toku_name":"'.$this->changeSpace(mb_convert_encoding($value['TOKNA1'],"utf-8","SJIS-win")).'"';
      $json .= ',"toku_cd":"'.$this->changeSpace(mb_convert_encoding($value['TOKCD'],"utf-8","SJIS-win")).'"';
      $json .= ',"irisuu":"'.number_format($value['IRISUU']).'"';
      $json .= ',"zaiko":"'.number_format($value['ZAIKO']).'"';
      $json .= ',"hakosuu":"'.number_format($value['HAKOSUU']).'"';
      $json .= '}';
      $index++;
    }
    $json .= ']';
    $json .= '}';

    echo $json;

  }
  function escapeHinmei($str) {
    $rtnStr = str_replace("\\","",$str);
    return $rtnStr;
  }
  function changeSpace($str) {
    $rtnStr = $str;
    if($str=="") {
      $rtnStr = "&nbsp;";
    }
    return $rtnStr;
  }
}
?>
