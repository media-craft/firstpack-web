<?php
/**
 * @file	ログインコントローラー
 * @author	kanda@media-craft.co.jp
 * @date	2018/02/17
 * @version	1.00
 * @note	ログイン時のリクエストを振り分ける
 */

require_once("Controller/baseController.php");
require_once("Model/Data.php");

class tokuisakiController extends baseController {

	/**
	 * コンストラクタ
	 */
	public function __construct(){
		parent::__construct();
	}
	/**
	 * デストラクタ
	 */
	public function __destruct(){
	}
	/**
	 * 得意先一覧の取得
	 * @return    なし
	 */
	public function listAction() {
		$title = SystemConfig::$SYSNAME;
    require_once("Lib/Session.php");
    $session = new Session();
    $session->start();
    $siten_cd = $session->get('siten_cd');
    $where = array();

    $where['siten_cd'] = $siten_cd;
    if($_REQUEST['toku_name']!=""){
      $where['toku_name'] = "%".mb_convert_encoding($_REQUEST['toku_name'],"SJIS-win","utf-8")."%";
    }
    if($_REQUEST['tel']!=""){
      $where['tel'] = "%".str_replace("-","",$_REQUEST['tel'])."%";
    }
    //得意先一覧の取得
    require_once("Model/Tokuisaki.php");
    $tokuisaki = new Tokuisaki();
    $tokuList = $tokuisaki->getTokuList($where);

    //jsonの生成
    $json = '';
    $json .= '{';
    $json .= '"head":{';
    $json .= '"status":1';
    $json .= '}';
    $json .= ',"data":[';
    $index = 1;
    foreach($tokuList as $key => $value) {
      if($index!=1) {
        $json .= ',';
      }
      $json .= '{';
      $json .= '"cd":"'.$value['CD'].'"';
      $json .= ',"name":"'.mb_substr(mb_convert_encoding($value['NA1'],"utf-8","SJIS-win"),0,5).'"';
      $json .= ',"ryaku":"'.mb_convert_encoding($value['RYAKU'],"utf-8","SJIS-win").'"';
      $json .= '}';
      $index++;
    }
    $json .= ']';
    $json .= '}';

    echo $json;
	}
}
?>
