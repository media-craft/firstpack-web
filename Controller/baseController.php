<?php
/**
 * @file	基本コントローラー
 * @author	kanda@media-craft.co.jp
 * @date	2018/02/17
 * @version	1.00
 * @note	すべてのコントローラーの基本となるクラス
 */

require_once("Lib/SystemConfig.php");
require_once("Lib/Log.php");
require_once("Lib/Session.php");

class baseController {
  protected $LOG = null;
	private $request;
	public  $session;
	/**
	 * コンストラクタ
	 */
	public function __construct(){
    //$this->initialize();
    global $LOG;
    $LOG = new Log();
	}

	public function sessionCheck() {
		$this->session = new Session();
		$this->session->start();
		if(!$this->session->sessionCheck()){
			$this->session->close();
			$url = SystemConfig::$SYSURL.SystemConfig::$SYSFLD."/";
			header("Location:$url");
			exit;
		}
	}
}
?>
