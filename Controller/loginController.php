<?php
/**
 * @file	ログインコントローラー
 * @author	kanda@media-craft.co.jp
 * @date	2018/02/17
 * @version	1.00
 * @note	ログイン時のリクエストを振り分ける
 */

require_once("Controller/baseController.php");
require_once("Model/Login.php");

class loginController extends baseController {

	/**
	 * コンストラクタ
	 */
	public function __construct(){
		parent::__construct();
	}
	/**
	 * デストラクタ
	 */
	public function __destruct(){
	}
	/**
	 * ログイン画面初期表示
	 * @return    なし
	 */
	public function indexAction() {
		$title = SystemConfig::$SYSNAME;
		//Viewの呼出
		include("./View/login/index.php");
	}
	/**
	 * ログイン開始
	 * @return    なし
	 */
	public function startAction() {

		$login = new Login();
    $where = array();
    $where = array("cd"=>$_POST['cd'],"pass_word"=>$_POST['pass_word']);
    $row = $login->checkLogin($where);

    $res = FALSE;

    if($row) {
      //端末IDが登録されているか確認
      $kisyu_id = $row[0]['KISYU_ID'];
      $cd       = $row[0]['CD'];

      if($kisyu_id=="") {
        //空の場合生成して登録
        $kisyu_id = $login->createTermId();
        if($kisyu_id!="") {
          //DBへ端末IDを書込
          if($login->writeTermId($cd,$kisyu_id)) {
            //cookieに書込
            setcookie('terminal_id',$kisyu_id,time()+86400 * 365 * 10); //有効期限を10年に
            $res = TRUE;
          }
        }
      } else {
        //DBの端末IDとcookieに書き込まれている端末IDを比較
        if($kisyu_id==$_COOKIE['terminal_id']) {
          $res = TRUE;
        }
      }
    }
    if($res) {
      //ログイン成功
      $session = new Session();
      $session->start();
      $session->set('login',true);
      $session->set('cd',$row[0]['CD']);
      $session->set('siten_cd',$row[0]['SITEN_CD1']);
      echo 'OK';
    } else {
      //ログイン失敗
      echo 'NG';
    }
	}
  /**
  * ログアウト処理
  **/
  public function outAction() {
    $session = new Session();
    $session->close();
    //ログイン画面へ遷移
    $url = SystemConfig::$SYSURL;
    header("Location:$url");
    exit;
  }
}
?>
