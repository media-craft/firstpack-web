<?php
/**
 * @file	データベースクラス
 * @author	kanda@media-craft.co.jp
 * @date	2018/02/17
 * @version	1.00
 * @note	データベースに関する処理を行うクラス
 */

require_once("Lib/SystemConfig.php");
require_once("Lib/Log.php");
class DatabaseAccess {

	//DB接続情報
	private $DSN = "";
	private $USER = "";
	private $PASS = "";
	private $con  = "";

	private $log  = "";

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		//ログインスタンスの作成
		$this->log = new Log();
	}

	/**
	 * デストラクタ
	 */
	public function __destruct()
	{
		// 取得した各情報を初期化
		$this->DSN = "";
		$this->USER = "";
		$this->PASS = "";
	}
	/**
	 * DB接続
	 * return bool
	 */
	public function connect() {
		try {
			$tns = "(DESCRIPTION =
				 		(ADDRESS =
						 	(PROTOCOL = TCP)
							(HOST = 192.168.254.36)
							(PORT = 1521)
						)
						(CONNECT_DATA =
					 		(SERVER = DEDICATED)
							(SERVICE_NAME = orcl)
						)
					)";
			$this->con = new PDO('oci:dbname='.$tns,'FIRSTPACK','FIRSTPACK');
      $this->log->trace('DB CONNECT SUCCESS');
			return $this->con;
		} catch(Exception $e) {
			$this->log->trace($e->getMessage());
			return false;
		}
	}
  /**
   * SQL実行
   * return statement
   */
   public function query($sql,$params){
    try {
      if(!$this->isConnect()) {
        $this->connect();
      }
      $stmt = $this->con->prepare($sql,$params);
      $result = $stmt->execute($params);
    } catch(Exception $e) {
      $this->log->trace($e->getMessage());
      return FALSE;
    }
    return $stmt;
  }
  /*
  * 接続確認
  * return:TRUE/FALSE
  */
  private function isConnect() {
    if(!$this->con){
      if(!$this->connect()){
        return FALSE;
      }
    }
    return TRUE;
  }
}
?>
