<?php
/**
 * @file	Sessionクラス
 * @author	kanda@media-craft.co.jp
 * @date	2018/10/05
 * @version	1.00
 * @note	セッションの管理を行うクラス
 */
require_once("Lib/SystemConfig.php");

class Session {
    /*
    * コンストラクタ
    */
    public function __construct()
    {

    }
    /*
    * デストラクタ
    */
    public function __destruct()
    {

    }
    /*
    * セッション開始
    */
    public function start()
    {
      if(!isset($_SESSION))
      {
        session_name(SystemConfig::$SESSION_NAME);
        session_regenerate_id();
        session_start();
      }
    }
    /*
    * セッション開始
    */
    public function close()
    {
      if(isset($_SESSION))
      {
        $_SESSION = array();
        setcookie(session_name(SystemConfig::$SESSION_NAME), '', time() - 3600);
        session_destroy();
      }
    }
    /*
    * セッションデータ取得
    * reutrn:セッションの値
    */
    public function get($key)
    {
      return $_SESSION[$key];
    }
    /*
    * セッションデータ保存
    */
    public function set($key,$value)
    {
      $_SESSION[$key] = $value;
    }
    /*
    * セッションデータ削除
    */
    public function delete($key)
    {
      unset($_SESSION[$key]);
    }
    /*
  	* セッションの値のセット
  	*/
  	public function sessionCheck() {
  		$rtnFlg = false;
  		$this->start();
  		if($this->get('login')) {
  			$rtnFlg = true;
  		}
  		return $rtnFlg;
  	}
}
?>
