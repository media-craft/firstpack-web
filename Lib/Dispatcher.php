<?php
/**
 * @file	Dispatcherクラス
 * @author	kanda@media-craft.co.jp
 * @date	2018/02/17
 * @version	1.00
 * @note	リクエストパラメータ振り分けクラス
 */
class Dispatcher {
	/**
	 * リクエストの振り分け処理
	 */
	public function dispatch(){
		require_once("Lib/SystemConfig.php");
		//リクエスト振り分け
		$exp = SystemConfig::$SYSFLD;
		$params = array();
		$params = explode("/",str_replace($exp,"",$_SERVER['REQUEST_URI']));

		if($params[1]=="" || $params[2]=="") {
			$url = SystemConfig::$SYSURL.SystemConfig::$SYSFLD."/login/index";
			header("Location:$url");
			exit;
		}
		//コントローラー呼出
		if($params[1]!="") {
			$controller = $params[1];
		}
    //アクションの振り分け
    if($params[2]!="") {
      $action = strtolower($params[2]);
    }


		$className = strtolower($controller) . 'Controller';
		require_once(SystemConfig::$SYSPATH."/../Controller/".$className.".php");
		//インスタンス作成
		$instance = new $className();

    if($controller!='login' && action!='index') {
      $instance -> sessionCheck();
    }


		$actionMethod = $action."Action";
		$instance -> $actionMethod();

	}
}
?>
