<?php
/**
 * @file	システム設定クラス
 * @author	kanda@media-craft.co.jp
 * @date	2018/10/05
 * @version	1.00
 * @note	システムの設定情報を保存
 */

class SystemConfig {
	//システム名
	public static $SYSNAME = "ファーストパック WEBシステム";
	//システムパス
	public static $SYSPATH = __DIR__;
	//システムURL
	public static $SYSURL  = "http://192.168.254.81";
	//public static $SYSURL  = "http://192.168.254.46";

  //public static $SYSURL  = "https://first-pack.net";

	//システムフォルダURL
	public static $SYSFLD  = "";

	//ログ吐き出しフォルダ
	public static $LOGFLD   = "d:/wwwroot/firstpack-web/Log";
	//public static $LOGFLD   = "/var/www/html/Log";
	//ログレベル
	//public static $LOG_LEVEL = "1";	//traceのみ
	public static $LOGLEVEL = "3";	//trace、debug

  //セッション名
  public static $SESSION_NAME = "iVQY2gPixZAkCEwACHaE77KYUrake8pn";
}
?>
