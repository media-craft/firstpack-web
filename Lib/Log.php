<?php
/**
 * @file	ログクラス
 * @author	kanda@media-craft.co.jp
 * @date	2018/02/17
 * @version	1.00
 * @note	ログ書き出しに関するクラス
 */

require_once("Lib/SystemConfig.php");

class Log {
	
	//ログ保存先
	private $savePath = "";
	//書き込みレベル
	private $logLevel = "";
	
	/**
	 * コンストラクタ
	 */
	public function __construct(){
		$this->savePath = SystemConfig::$LOGFLD;
		$this->logLevel = SystemConfig::$LOGLEVEL;
	}

	/**
	 * デストラクタ
	 */
	public function __destruct(){
	}
	
	/**
	 * ログ出力(通常)
	 * return:void
	 */
	public function trace($msg) {
		$txt  = date("Y/m/d H:i:s")."\r\n";
		$txt .= "Trace:".$msg;
		$filename = date("Ymd").".log";
		$fp = @fopen($this->savePath."/".$filename,"a");
		flock($fp,LOCK_EX);		//排他
		fwrite($fp,"\r\n".$txt);
		flock($fp,LOCK_UN);		//ロック解除
		fclose($fp);
	}
	/**
	 * ログ出力(デバッグ時のみ)
	 * return:void
	 */
	public function debug($msg) {
		
		if($this->logLevel < 3) {
			return;
		}
		
		$txt  = date("Y/m/d H:i:s")."\r\n";
		$txt .= "Debug:".$msg;
		$filename = date("Ymd").".log";
		$fp = @fopen($this->savePath."/".$filename,"a");
		flock($fp,LOCK_EX);		//排他
		fwrite($fp,"\r\n".$txt);
		flock($fp,LOCK_UN);		//ロック解除
		fclose($fp);
	}
}
?>