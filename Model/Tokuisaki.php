<?php
/**
 * @file    TOKUISAKIモデル
 * @author  kanda@media-craft.coljp
 * @date    2018/10/05
 * @version 1.00
 * @note    TIRE_M_TOKUモデルの処理を定義
 */

require_once("Model/baseModel.php");

class Tokuisaki extends baseModel {
	/**
	 * コンストラクタ
	 */
	public function __construct(){
		parent::__construct();
	}
	/**
	 * デストラクタ
	 */
	public function __destruct(){

	}
  /**
   * 得意先一覧の取得
   * return array
   */
  public function getTokuList($where=array()) {
    global $DB;
		$sql  = " SELECT * FROM TIRE_M_TOKU";
		$sql .= " WHERE NVL(siten_cd,'0') = :siten_cd";
    if($where['tel']!="") {
      $sql .= " AND REPLACE(tel1,'-','') LIKE :tel";
    }
    if($where['toku_name']!="") {
      $sql .= " AND na1 LIKE :toku_name";
    }
		$sql .= " ORDER by cd";
    try {
  		$stmt = $DB->query($sql,$where);
  		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch(Exception $e) {
      $LOG->trace($e->getMessage());
      return FALSE;
    }
		return $rows;
  }
}

?>
