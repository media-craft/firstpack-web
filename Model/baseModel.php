<?php
/**
 * @file    共通モデルクラス
 * @author  kanda@media-craft.coljp
 * @date    2018/02/17
 * @version 1.00
 * @note    モデルクラスの共通処理を定義
 */
require_once("Lib/DatabaseAccess.php");
require_once("Lib/Log.php");
require_once("Lib/Session.php");
class baseModel {
	protected $DB  = null;
	protected $LOG = null;
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		global $DB;
		global $LOG;
		$DB  = new DatabaseAccess();
		$LOG = new Log();
	}
}
 
?>