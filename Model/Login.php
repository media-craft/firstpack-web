<?php
/**
 * @file    LOGINモデル
 * @author  kanda@media-craft.coljp
 * @date    2018/02/17
 * @version 1.00
 * @note    LOGINモデルの処理を定義
 */

require_once("Model/baseModel.php");

class Login extends baseModel {
	/**
	 * コンストラクタ
	 */
	public function __construct(){
		parent::__construct();
	}

	/**
	 * デストラクタ
	 */
	public function __destruct(){

	}

	public function checkLogin($where) {
		global $DB;
    global $LOG;
		$sql  = " select * from TIRE_M_TANTO";
		//$sql .= " where siten_kbn='1'";
		$sql .= " where cd = :cd";
		$sql .= " and pass_word = :pass_word";
    try {
  		$stmt = $DB->query($sql,$where);
  		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch(Exception $e) {
      $LOG->trace($e->getMessage());
      return FALSE;
    }
		return $rows;
	}

  /**
   * 端末IDの書込
   */
   public function writeTermId($cd,$kisyu_id) {
     global $DB;
     global $LOG;
     $rtnFlg = FALSE;
     //パラメータ不足の場合はエラーを返す
     if($cd=="" || $kisyu_id=="") {
       return $rtnFlg;
     }

     $params['cd']       = $cd;
     $params['kisyu_id'] = $kisyu_id;
     try {
       //書込
       $sql  = " UPDATE TIRE_M_TANTO SET ";
       $sql .= " kisyu_id=:kisyu_id";
       $sql .= " WHERE cd=:cd";
       $stmt = $DB->query($sql,$params);
     } catch(Exception $e) {
       $LOG->trace($e->getMessage());
       return FALSE;
     }
     if($stmt) {
       $rtnFlg = TRUE;
     }
     return $rtnFlg;
   }

  /**
   * ID生成
   */
   public function createTermId() {
     global $DB;
     global $LOG;
     $rtnStr = "";

     $loop = true;
     $index = 1;
     while($loop) {
       //ランダム文字列を生成
       $rtnStr = md5(uniqid(rand(), true));
       $LOG->trace($rtnStr);

       $where['kisyu_id'] = $rtnStr;

       $sql  = " SELECT";
       $sql .= " * ";
       $sql .= " FROM TIRE_M_TANTO";
       $sql .= " WHERE kisyu_id=:kisyu_id";
       try {
         $stmt = $DB->query($sql,$where);
     	   $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
       } catch(Exception $e) {
         $LOG->trace($e->getMessage());
         return "";
       }
       //生成したIDが重複していなければループから抜ける
       if(empty($rows)) {
         $loop =  false;
       }
       if($index>=100) {
         $LOG->trace('Loop Error');
         return "";
       }
       $index++;
     }
     return $rtnStr;
   }

}

?>
