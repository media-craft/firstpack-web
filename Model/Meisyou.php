<?php
/**
 * @file    MEISYOUモデル
 * @author  kanda@media-craft.coljp
 * @date    2018/10/05
 * @version 1.00
 * @note    TIRE_M_MEISYOUモデルの処理を定義
 */

require_once("Model/baseModel.php");

class Meisyou extends baseModel {
	/**
	 * コンストラクタ
	 */
	public function __construct(){
		parent::__construct();
	}
	/**
	 * デストラクタ
	 */
	public function __destruct(){

	}
  /**
   * 商品区分2の名称を取得
   * return array
   */
  public function getSyohinKbn2() {
    global $DB;
		$sql  = " SELECT * FROM TIRE_M_MEISYOU";
		$sql .= " WHERE kbn='6'";
		$sql .= " ORDER by cd";
    $where = array();
    try {
  		$stmt = $DB->query($sql,$where);
  		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch(Exception $e) {
      $LOG->trace($e->getMessage());
      return FALSE;
    }
		return $rows;
  }
}

?>
