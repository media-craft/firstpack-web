<?php
/**
 * @file    LOGINモデル
 * @author  kanda@media-craft.coljp
 * @date    2018/02/17
 * @version 1.00
 * @note    LOGINモデルの処理を定義
 */

require_once("Model/baseModel.php");

class Data extends baseModel {
	/**
	 * コンストラクタ
	 */
	public function __construct(){
		parent::__construct();
	}

	/**
	 * デストラクタ
	 */
	public function __destruct(){

	}
  /**
   * 在庫管理表のデータの取得
   * return array
   */
   public function getStockList($where) {
    global $DB;
    global $LOG;
    /*
    $sql  = " Select ";
    $sql .= "    SYO.CD, ";
    $sql .= "    SYO.NA1  as HINMEI, ";
    //$sql .= "    SYO.KIKAKU, ";
    //$sql .= "    SYO.TANI, ";
    $sql .= "    SYO.IRISUU, ";
    //$sql .= "    SYO.HACHU, ";
    //$sql .= "    SYO.ZAIKBN, ";
    //$sql .= "    SYO.SITEN_KBN, ";
    //$sql .= "    SYO.SITEN_CD, ";
    $sql .= "    SYO.TOKCD, ";
    //$sql .= "    BNR.NA1 as BUNRUINA, ";
    //$sql .= "    MEI.NA1 as MEISYOUNA, ";
    //$sql .= "    TOK.NA1 || ' ' || TOK.NA2 as TOKNA1, ";
    $sql .= "    TOK.RYAKU as TOKNA1, ";
    $sql .= "    NVL(SYO.ZAIKO,0) + NVL(URI3.SUU,0) - NVL(SIR3.SUU,0) + NVL(SYU3.SUU,0) - NVL(NYU3.SUU,0) as ZAIKO, ";
    $sql .= "    Trunc(Decode(SYO.IRISUU,0,0,NVL(SYO.ZAIKO,0) + NVL(URI3.SUU,0) - NVL(SIR3.SUU,0) + NVL(SYU3.SUU,0) - NVL(NYU3.SUU,0) / SYO.IRISUU)) as HAKOSUU ";
    $sql .= " from TIRE_M_SYOHIN SYO ";
    $sql .= "    , TIRE_M_MEISYOU BNR ";
    $sql .= "    , TIRE_M_MEISYOU MEI ";
    $sql .= "    , TIRE_M_TOKU TOK ";
    $sql .= "    ,(Select SYOCD, SUM(SUU) as SUU from TIRE_T_URITRN   Where YMD > :ymd and GYOKBN <> '2' Group By SYOCD) URI3 ";
    $sql .= "    ,(Select SYOCD, SUM(SUU) as SUU from TIRE_T_SIRTRN   Where YMD > :ymd Group By SYOCD) SIR3 ";
    $sql .= "    ,(Select SYOCD, SUM(SUU) as SUU from TIRE_T_NYUKATRN Where YMD > :ymd and (ZAIKBN = 0 or ZAIKBN = 2) Group By SYOCD) NYU3 ";
    $sql .= "    ,(Select SYOCD, SUM(SUU) as SUU from TIRE_T_KAKOUTRN Where YMD > :ymd and (ZAIKBN = 0 or ZAIKBN = 2) Group By SYOCD) SYU3 ";
    $sql .= " Where  ";
    $sql .= "       NVL(SYO.SITEN_CD,'0') = :siten_cd ";

    //$sql .= " and   SYO.ZAIKBN  >= :ZAIKBN1 ";
    //$sql .= " and   SYO.ZAIKBN  <= :ZAIKBN2 ";

    if($where['zaiko_kbn']!="") {
      $sql .= " and   SYO.ZAIKBN  = :zaiko_kbn ";
    }
    if($where['syo_kbn2']!="") {
      $sql .= " and   NVL(SYO.KBN_CD2,' ') >= :syo_kbn2 ";
    }

    //$sql .= " and   NVL(SYO.KBN_CD2,' ') >= :SYOKBN1 ";
    //$sql .= " and   NVL(SYO.KBN_CD2,' ') <= :SYOKBN2 ";

    $sql .= " and   NVL(SYO.KBN_CD1,' ') = BNR.CD(+) ";
    $sql .= " and   NVL(SYO.KBN_CD2,' ') = MEI.CD(+) ";
    $sql .= " and   NVL(SYO.TOKCD,' ')   = TOK.CD(+) ";
    $sql .= " and   5 = BNR.KBN(+) ";
    $sql .= " and   6 = MEI.KBN(+) ";
    $sql .= " and   SYO.CD       = URI3.SYOCD(+) ";
    $sql .= " and   SYO.CD       = SIR3.SYOCD(+) ";
    $sql .= " and   SYO.CD       = SYU3.SYOCD(+) ";
    $sql .= " and   SYO.CD       = NYU3.SYOCD(+) ";
    $sql .= " Order By 1 ";
    */
    $sql  = " Select ";
    $sql .= "    SYO.CD, ";
    $sql .= "    SYO.NA1  as HINMEI, ";
//    $sql .= "    SYO.KIKAKU, ";
//    $sql .= "    SYO.TANI, ";
    $sql .= "    SYO.IRISUU, ";
//    $sql .= "    SYO.HACHU, ";
//    $sql .= "    SYO.ZAIKBN, ";
//    $sql .= "    SYO.SITEN_KBN, ";
//    $sql .= "    SYO.SITEN_CD, ";
    $sql .= "    SYO.TOKCD, ";
//    $sql .= "    BNR.NA1 as BUNRUINA, ";
//    $sql .= "    MEI.NA1 as MEISYOUNA, ";
//    $sql .= "    TOK.NA1 || ' ' || TOK.NA2 as TOKNA1, ";
    $sql .= "    TOK.RYAKU as TOKNA1, ";
    $sql .= "    NVL(SYO.ZAIKO,0) + NVL(URI3.SUU,0) - NVL(SIR3.SUU,0) + NVL(SYU3.SUU,0) - NVL(NYU3.SUU,0) as ZAIKO, ";
    $sql .= "    Trunc(Decode( ";
    $sql .= "          NVL(SYO.IRISUU,0),0,0 ";
    $sql .= "         ,(NVL(SYO.ZAIKO,0) + NVL(URI3.SUU,0) - NVL(SIR3.SUU,0) + NVL(SYU3.SUU,0) - ";
    $sql .= " NVL(NYU3.SUU,0)) / SYO.IRISUU)) as HAKOSUU ";
    $sql .= " from TIRE_M_SYOHIN SYO ";
    $sql .= "    , TIRE_M_MEISYOU BNR ";
    $sql .= "    , TIRE_M_MEISYOU MEI ";
    $sql .= "    , TIRE_M_TOKU TOK ";
    $sql .= "    ,(Select SYOCD, SUM(SUU) as SUU from TIRE_T_URITRN   Where YMD > :ymd and  ";
    $sql .= " GYOKBN <> '2' Group By SYOCD) URI3 ";
    $sql .= "    ,(Select SYOCD, SUM(SUU) as SUU from TIRE_T_SIRTRN   Where YMD > :ymd Group By  ";
    $sql .= " SYOCD) SIR3 ";
    $sql .= "    ,(Select SYOCD, SUM(SUU) as SUU from TIRE_T_NYUKATRN Where YMD > :ymd and (ZAIKBN = 0 or ZAIKBN = 2) Group By SYOCD) NYU3 ";
    $sql .= "    ,(Select SYOCD, SUM(SUU) as SUU from TIRE_T_KAKOUTRN Where YMD > :ymd and (ZAIKBN = 0 or ZAIKBN = 2) Group By SYOCD) SYU3 ";
    $sql .= " Where  ";
    $sql .= "       ( NVL(SYO.SITEN_CD,'0') = :siten_cd or SYO.SITEN_KBN = 0) ";
    if($where['toku_cd']!="") {
      $sql .= " and   SYO.TOKCD  = :toku_cd ";
    }
    /*
    $sql .= "       SYO.ZAIKBN  >= :ZAIKBN1 ";
    $sql .= " and   SYO.ZAIKBN  <= :ZAIKBN2 ";
    */
    /*
    $sql .= " and   NVL(SYO.KBN_CD2,' ') >= :SYOKBN1 ";
    $sql .= " and   NVL(SYO.KBN_CD2,' ') <= :SYOKBN2 ";
    */
    if($where['zaiko_kbn']!="") {
      $sql .= " and   SYO.ZAIKBN  = :zaiko_kbn ";
    }
    if($where['syo_kbn2']!="") {
      $sql .= " and   NVL(SYO.KBN_CD2,' ') >= :syo_kbn2 ";
    }

    $sql .= " and   NVL(SYO.KBN_CD1,' ') = BNR.CD(+) ";
    $sql .= " and   NVL(SYO.KBN_CD2,' ') = MEI.CD(+) ";
    $sql .= " and   NVL(SYO.TOKCD,' ')   = TOK.CD(+) ";
    $sql .= " and   5 = BNR.KBN(+) ";
    $sql .= " and   6 = MEI.KBN(+) ";
    $sql .= " and   SYO.CD       = URI3.SYOCD(+) ";
    $sql .= " and   SYO.CD       = SIR3.SYOCD(+) ";
    $sql .= " and   SYO.CD       = SYU3.SYOCD(+) ";
    $sql .= " and   SYO.CD       = NYU3.SYOCD(+) ";
    $sql .= " Order By 1 ";
    //$LOG->trace($sql);

    try {
   	  $stmt = $DB->query($sql,$where);
   		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch(Exception $e) {
      $LOG->trace($e->getMessage());
      return FALSE;
    }
 		return $rows;
   }
}

?>
