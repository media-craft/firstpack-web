
  $(function() {
    $("#kbnArea1 ul li.list").click(function() {
      $("#kbn1").html($(this).html());
      $('.modal-overlay').remove();
      $("#kbnArea1").css('display','none');
      $("#syo_kbn2").val($(this).attr('data-kbn'));
    });
    $("#kbnArea2 ul li.list").click(function() {
      $("#kbn2").html($(this).html());
      $('.modal-overlay').remove();
      $("#kbnArea2").css('display','none');
      $("#suu_kbn").val($(this).attr('data-kbn'));
    });
    $("#kbnArea3 ul li.list").click(function() {
      $("#kbn3").html($(this).html());
      $('.modal-overlay').remove();
      $("#kbnArea3").css('display','none');
      $("#zaiko_kbn").val($(this).attr('data-kbn'));
    });
  });
  function tokuSelected(cd,name) {
    $("#tokuName").html(name);
    $('.modal-overlay').remove();
    $("#tokuArea").css('display','none');
    $("#toku_cd").val(cd);
  }
  function showKbnArea1() {
    $('body').append('<div class="modal-overlay"></div>');
    $('.modal-overlay').fadeIn('slow');

    //var modal = '#' + $(this).attr('data-target');
    var modal = '#kbnArea1';
    $(modal).css('display','block');
    modalResize();
    $(modal).fadeIn('slow');

    $('.modal-overlay').off().click(function() {
      $(modal).css('display','none');
      $('.modal-overlay').fadeOut('slow',function() {
        $('.modal-overlay').remove();
      });
    });
    function modalResize() {
      var w = $(window).width();
      var x = (w - $(modal).outerWidth(true)) / 2;
      //var height = $(window).height()-50;
      //$("#kbnArea2").css('height',height);
      y = 30;
      $(modal).css({'left': x + 'px','top': y + 'px'});
    }
  }
  function showKbnArea2() {
    $('body').append('<div class="modal-overlay"></div>');
    $('.modal-overlay').fadeIn('slow');

    //var modal = '#' + $(this).attr('data-target');
    var modal = '#kbnArea2';
    $(modal).css('display','block');
    modalResize();
    $(modal).fadeIn('slow');

    $('.modal-overlay').off().click(function() {
      $(modal).css('display','none');
      $('.modal-overlay').fadeOut('slow',function() {
        $('.modal-overlay').remove();
      });
    });
    function modalResize() {
      var w = $(window).width();
      var x = (w - $(modal).outerWidth(true)) / 2;
      //var height = $(window).height()-50;
      //$("#kbnArea2").css('height',height);
      y = 150;
      $(modal).css({'left': x + 'px','top': y + 'px'});
    }
  }
  function showKbnArea3() {
    $('body').append('<div class="modal-overlay"></div>');
    $('.modal-overlay').fadeIn('slow');

    //var modal = '#' + $(this).attr('data-target');
    var modal = '#kbnArea3';
    $(modal).css('display','block');
    modalResize();
    $(modal).fadeIn('slow');

    $('.modal-overlay').off().click(function() {
      $(modal).css('display','none');
      $('.modal-overlay').fadeOut('slow',function() {
        $('.modal-overlay').remove();
      });
    });
    function modalResize() {
      var w = $(window).width();
      var x = (w - $(modal).outerWidth(true)) / 2;
      //var height = $(window).height()-50;
      //$("#kbnArea2").css('height',height);
      y = 150;
      $(modal).css({'left': x + 'px','top': y + 'px'});
    }
  }
  function showTokuArea() {
    $('body').append('<div class="modal-overlay"></div>');
    $('.modal-overlay').fadeIn('slow');

    //var modal = '#' + $(this).attr('data-target');
    var modal = '#tokuArea';
    $(modal).css('display','block');
    modalResize();
    $(modal).fadeIn('slow');

    $('.modal-overlay').off().click(function() {
      $(modal).css('display','none');
      $('.modal-overlay').fadeOut('slow',function() {
        $('.modal-overlay').remove();
      });
    });
    function modalResize() {
      var w = $(window).width();
      var x = (w - $(modal).outerWidth(true)) / 2;
      var height = $(window).height()-200;
      $("#tokuArea").css('height',height);
      y = 10;
      $(modal).css({'left': x + 'px','top': y + 'px'});
    }
  }
