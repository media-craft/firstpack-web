<?php
/**
 * @file	リクエスト振り分け
 * @author	kanda@media-craft.co.jp
 * @date	2018/02/17
 * @version	1.00
 * @note	システムに対する振り分けを行う
 */




//$path = "d:/wwwroot/firstpack-web/";
$path = "/var/www/html/";
ini_set('include_path',get_include_path() .PATH_SEPARATOR. $path);

require_once("Lib/Dispatcher.php");
$dispatcher = new Dispatcher();
$dispatcher -> dispatch();

?>
