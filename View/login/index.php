<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, user-scalable=no">
<meta name="robots" content="noindex,nofollow">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link rel="stylesheet" href="../../css/contents.css" media="all">
<script type="text/javascript" src="../../js/contents.js"></script>
<title>ログイン</title>
<script>
$(function() {
  $('.errorMessage').css('display','none');
  $("#loginBtn").click(function() {
    console.log('ログインボタン押下');
    $('.errorMessage').css('display','none');
    cd = $("#cd").val();
    pass_word = $("#pass_word").val();
    if(cd=="" || pass_word=="") {
      $('.errorMessage').css('display','');
    } else {
      $.ajax({
        url: "/login/start",
        type: "POST",
        data: {cd:cd,pass_word:pass_word},
        success: function(rtnData) {
          console.log(rtnData);
          if(rtnData=="OK") {
            url = '../data/stocklist/';
            location.href = url;
          } else {$('.errorMessage').css('display','');
            $('.errorMessage').css('display','');
          }
        },
        error:function(xhr, textStatus, error) {
          alert("通信エラー");
        }
      });
    }
  });
});
</script>
</head>
<body>
	<div id="container">
		<div id="loginOverlay">
			<div id="loginArea">
			<p class="title">担当者コードとパスワードを入力してください</p>
			<ul>
			<li><input type="number" class="size30" placeholder="担当者コード" name="cd" id="cd" value="<?php echo $_POST['cd'];?>"></li>
			<li class="mt10"><input type="password" class="size30" placeholder="パスワード" name="pass_word" id="pass_word"></li>
			</ul>
			<p class="errorMessage mtz">ログイン情報に誤りがあります</p>
			<button id="loginBtn">ログイン</button>
      <!--
			<p class="forgotten">パスワードを忘れた方は<a href="#">こちら</a></p>
      -->
			</div>
		</div>
	</div>
</body>
</html>
